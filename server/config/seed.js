/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';

var Thing = require('../api/thing/thing.model');
var Medication = require('../api/medication/medication.model');
var User = require('../api/user/user.model');
var Schedule = require('../api/schedule/schedule.model');

//Thing.find({}).remove(function() {
//  Thing.create({
//    name : 'Development Tools',
//    info : 'Integration with popular tools such as Bower, Grunt, Karma, Mocha, JSHint, Node Inspector, Livereload, Protractor, Jade, Stylus, Sass, CoffeeScript, and Less.'
//  }, {
//    name : 'Server and Client integration',
//    info : 'Built with a powerful and fun stack: MongoDB, Express, AngularJS, and Node.'
//  }, {
//    name : 'Smart Build System',
//    info : 'Build system ignores `spec` files, allowing you to keep tests alongside code. Automatic injection of scripts and styles into your index.html'
//  },  {
//    name : 'Modular Structure',
//    info : 'Best practice client and server structures allow for more code reusability and maximum scalability'
//  },  {
//    name : 'Optimized Build',
//    info : 'Build process packs up your templates as a single JavaScript payload, minifies your scripts/css/images, and rewrites asset names for caching.'
//  },{
//    name : 'Deployment Ready',
//    info : 'Easily deploy your app to Heroku or Openshift with the heroku and openshift subgenerators'
//  });
//});

Medication.find({}).remove(function() {
  Medication.create({
    medicationId: 1,
    name: 'Debridat',
    expirationDate: new Date('2015-10-10'),
    buyDate: new Date('2014-07-20'),
    description: 'For a cold smashed casserole, add some emeril\'s essence and brown sugar.',
    archive: false,
    similar:[2],
    antibiotic:true,
    catarrh:true,
    cough:false,
    fever:false,
    userType:1
  },
  {
    medicationId: 2,
    name: 'Herbitussin',
    expirationDate: new Date('2015-02-15'),
    buyDate: new Date('2014-09-11'),
    description: 'Waves grow from hungers like stormy reefs.',
    archive: false,
    similar:[1,3],
    antibiotic:false,
    catarrh:false,
    cough:true,
    fever:false,
    userType:2
  },
  {
    medicationId: 3,
    name: 'Pulneo',
    expirationDate: new Date('2015-03-15'),
    buyDate: new Date('2014-12-01'),
    description: 'Harmless, interstellar crewmates rudely avoid a crazy, evil klingon.',
    archive: false,
    similar:[2],
    antibiotic:false,
    catarrh:true,
    cough:true,
    fever:false,
    userType:3
  },
  {
    medicationId: 4,
    name: 'Ibum',
    expirationDate: new Date('2015-08-15'),
    buyDate: new Date('2015-02-01'),
    description: 'Belay, jolly.How black. You blow like a mate.',
    archive: false,
    similar:[2],
    antibiotic:false,
    catarrh:false,
    cough:true,
    fever:true,
    userType:3

  },
    {
      medicationId: 5,
      name: 'Debridat 2',
      expirationDate: new Date('2015-10-10'),
      buyDate: new Date('2014-07-20'),
      description: 'For a cold smashed casserole, add some emeril\'s essence and brown sugar.',
      archive: true,
      similar:[2],
      antibiotic:true,
      catarrh:true,
      cough:false,
      fever:false,
      userType:1
    }
  )
});

User.find({}).remove(function () {
    User.create({
            provider: 'local',
            name: 'Test User',
            email: 'test@test.com',
            password: 'test'
        }, {
            provider: 'local',
            role: 'admin',
            name: 'Mateutek',
            email: 'mateutek.w@gmail.com',
            password: '12341234'
        }, {
            provider: 'local',
            role: 'admin',
            name: 'Admin',
            email: 'admin@admin.com',
            password: 'admin'
        }, function () {
            console.log('finished populating users');
        }
    );
});


Schedule.find({}).remove(function () {
  Schedule.create(
    {
      scheduleId:1,
      name:'Test',
      description:'Acceptance happens when you understand everything so truly that whatsoever you are doing is your tantra.',
      active:true,
      person:'Mateusz',
      startDate: new Date(),
      endDate: new Date('2015-05-27')
    }
  );
});
