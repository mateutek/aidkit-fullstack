'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ScheduleItemSchema = new Schema({
  name: String,
  info: String,
  active: Boolean
});

module.exports = mongoose.model('ScheduleItem', ScheduleItemSchema);