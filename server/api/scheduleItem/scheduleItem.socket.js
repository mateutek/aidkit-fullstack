/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var ScheduleItem = require('./scheduleItem.model');

exports.register = function(socket) {
  ScheduleItem.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  ScheduleItem.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('scheduleItem:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('scheduleItem:remove', doc);
}