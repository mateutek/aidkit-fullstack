'use strict';

var _ = require('lodash');
var ScheduleItem = require('./scheduleItem.model');

// Get list of scheduleItems
exports.index = function(req, res) {
  ScheduleItem.find(function (err, scheduleItems) {
    if(err) { return handleError(res, err); }
    return res.json(200, scheduleItems);
  });
};

// Get a single scheduleItem
exports.show = function(req, res) {
  ScheduleItem.findById(req.params.id, function (err, scheduleItem) {
    if(err) { return handleError(res, err); }
    if(!scheduleItem) { return res.send(404); }
    return res.json(scheduleItem);
  });
};

// Creates a new scheduleItem in the DB.
exports.create = function(req, res) {
  ScheduleItem.create(req.body, function(err, scheduleItem) {
    if(err) { return handleError(res, err); }
    return res.json(201, scheduleItem);
  });
};

// Updates an existing scheduleItem in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  ScheduleItem.findById(req.params.id, function (err, scheduleItem) {
    if (err) { return handleError(res, err); }
    if(!scheduleItem) { return res.send(404); }
    var updated = _.merge(scheduleItem, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, scheduleItem);
    });
  });
};

// Deletes a scheduleItem from the DB.
exports.destroy = function(req, res) {
  ScheduleItem.findById(req.params.id, function (err, scheduleItem) {
    if(err) { return handleError(res, err); }
    if(!scheduleItem) { return res.send(404); }
    scheduleItem.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}