'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ScheduleSchema = new Schema({
  scheduleId: Number,
  name: String,
  description: String,
  active: Boolean,
  person: String,
  startDate: Date,
  endDate: Date
});

module.exports = mongoose.model('Schedule', ScheduleSchema);
