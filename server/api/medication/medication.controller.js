'use strict';

var _ = require('lodash');
var Medication = require('./medication.model');

// Get list of medications
exports.index = function(req, res) {
  Medication.find({archive:0},function (err, medications) {
    if(err) { return handleError(res, err); }
    return res.json(200, medications);
  });
};

// Get a single medication
exports.show = function(req, res) {
  Medication.findById(req.params.id, function (err, medication) {
    if(err) { return handleError(res, err); }
    if(!medication) { return res.send(404); }
    return res.json(medication);
  });
};

// Creates a new medication in the DB.
exports.create = function(req, res) {
  Medication.create(req.body, function(err, medication) {
    if(err) { return handleError(res, err); }
    return res.json(201, medication);
  });
};

// Updates an existing medication in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Medication.findById(req.params.id, function (err, medication) {
    if (err) { return handleError(res, err); }
    if(!medication) { return res.send(404); }
    var updated = _.merge(medication, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, medication);
    });
  });
};

// Deletes a medication from the DB.
exports.destroy = function(req, res) {
  Medication.findById(req.params.id, function (err, medication) {
    if(err) { return handleError(res, err); }
    if(!medication) { return res.send(404); }
    medication.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

exports.find = function(req,res) {
  Medication.find({name:{ "$regex": req.params.query, "$options": "i" } },function(err, medications){
    if(err) { return handleError(res, err); }
    if(!medications) { return res.send(404); }
    return res.json(medications);
  });
};

exports.findAdv = function(req,res) {
  var query = translateQuery(req.body);
  console.log(query);
  Medication.find({name:{ "$regex": req.body.query, "$options": "i" }}).where(query).exec(function(err, medications){
    if(err) { return handleError(res, err); }
    if(!medications) { return res.send(404); }
    return res.json(medications);
  });
};

function handleError(res, err) {
  return res.send(500, err);
}

function translateQuery(query){
  var tmpQuery = {
    $or:[],
    archive:false
  };
  if(query.searchFields.antibiotic || query.searchFields.catarrh || query.searchFields.cough ||query.searchFields.fever){
    if(typeof tmpQuery.$or === 'undefined'){
      tmpQuery.$or = [];
    }
    if(query.searchFields.antibiotic){
      tmpQuery.$or.push({antibiotic:true});
    }
    if(query.searchFields.catarrh){
      tmpQuery.$or.push({catarrh:true});
    }
    if(query.searchFields.cough){
      tmpQuery.$or.push({cough:true});
    }
    if(query.searchFields.fever){
      tmpQuery.$or.push({fever:true});
    }
  }else{
    delete tmpQuery.$or;
  }

  if(query.searchFields.adults || query.searchFields.kids){
    tmpQuery.userType={};
    if(typeof tmpQuery.userType.$in === 'undefined'){
      tmpQuery.userType.$in=[];
    }
    if(query.searchFields.adults){
      tmpQuery.userType.$in.push(2);
      tmpQuery.userType.$in.push(3);
    }
    if(query.searchFields.kids){
      tmpQuery.userType.$in.push(1);
    }
  }else{
    delete tmpQuery.userType;
  }

  if(query.searchFields.archive){
    delete tmpQuery.archive
  }else{
    tmpQuery.archive = false;
  }
  return tmpQuery;
}
