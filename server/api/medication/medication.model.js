'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var MedicationSchema = new Schema({
  medicationId:Number,
  name: String,
  expirationDate: Date,
  buyDate: Date,
  description: String,
  archive: Boolean,
  similar:[Number],
  antibiotic:Boolean,
  catarrh:Boolean,
  cough:Boolean,
  fever:Boolean,
  userType:Number
});

module.exports = mongoose.model('Medication', MedicationSchema);
