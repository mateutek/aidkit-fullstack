/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Medication = require('./medication.model');

exports.register = function(socket) {
  Medication.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Medication.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
};

function onSave(socket, doc, cb) {
  socket.emit('medication:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('medication:remove', doc);
}
