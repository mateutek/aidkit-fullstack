'use strict';

angular.module('aidkitApp')
    .controller('NavbarCtrl', function ($scope, $location, Auth ,$state) {
        $scope.menu = [
            {
                'title': 'Strona Główna',
                'link': '/'
            },
            {
              'title': 'Listy',
              'link': '/lists/all'
            },
            {
              'title': 'Rozpiski',
              'link': '/schedules/all'
            }
        ];

        $scope.isCollapsed = true;
        $scope.isLoggedIn = Auth.isLoggedIn;
        $scope.isAdmin = Auth.isAdmin;
        $scope.getCurrentUser = Auth.getCurrentUser;
        $scope.searchForm = {};
        $scope.searchForm.query = "";
        $scope.logout = function () {
            Auth.logout();
            $location.path('/login');
        };

        $scope.isActive = function (route) {
            return route === $location.path();
        };

        $scope.searchActive = function (){
            return $location.path().indexOf('/medication/') <= -1  ;
        };

        $scope.searchForm.searchMedications = function(item, event){
            $state.go('main.search',{'query':$scope.searchForm.query})
        }
    });
