'use strict';

describe('Directive: userType', function () {

  // load the directive's module and view
  beforeEach(module('aidkitApp'));
  beforeEach(module('components/userType/userType.html'));

  var element, scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<user-type></user-type>');
    element = $compile(element)(scope);
    scope.$apply();
    expect(element.text()).toBe('this is the userType directive');
  }));
});