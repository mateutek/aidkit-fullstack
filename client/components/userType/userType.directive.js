'use strict';

angular.module('aidkitApp')
  .directive('userType', function () {
    return {
      templateUrl: 'components/userType/userType.html',
      restrict: 'EA',
      link: function (scope, element, attrs) {
        var userType = {
          icon:'user',
          colorClass: 'text-info'
        };
        var getUserType = function(){
          switch (attrs.type){
            case '1':
              userType.icon='male';
              userType.colorClass = 'text-primary';
              break;
            case '2':
              userType.icon='female';
              userType.colorClass = 'text-purple';
              break;
            case '3':
              userType.icon='child';
              userType.colorClass = 'text-pink';
              break;
            default:
                userType.icon='user';
          }
          scope.userType = userType;
        };
        attrs.$observe("type", getUserType);
      }
    };
  });
