'use strict';

angular.module('aidkitApp')
    .factory('notifications', function ($location, $rootScope, $http, User, $cookieStore, $q) {
        // Service logic
        // ...

        var meaningOfLife = 42;

        // Public API here
        return {
            notify: function (message) {
                console.log(message);
            }
        };
    });
