'use strict';

angular.module('aidkitApp')
  .factory('aidkitMoment', function ($location, $rootScope, $http, User, $cookieStore, $q, moment) {
    return {
      getDiff: function (endDate,startDate) {
        var mStartDate = new moment(startDate),
            mEndDate = new moment(endDate),
            now = new moment(),
            //passed = now.diff(mStartDate),
            left = mEndDate.diff(now),
            total = mEndDate.diff(mStartDate),
            //dP = moment.duration(passed).asDays(),
            dL = moment.duration(left).asDays(),
            dT = moment.duration(total).asDays(),
            usability={};
        if(Math.floor(dL)<=0){
          usability.percentage = 100;
          usability.class = 'danger progress-bar-striped active';
          usability.text = 'Przeterminowany!';
        }else{
          usability.percentage =  (dL/dT)*100;
          if(usability.percentage <=10){
            usability.text = Math.round(dL);
            usability.class = 'danger';
          } else if(usability.percentage <=25){
            usability.text = 'Dni: '+Math.round(dL);
            usability.class = 'warning';
          } else if(usability.percentage <=50 ){
            usability.text = 'Dni: '+Math.round(dL);
            usability.class = 'info'
          } else if(usability.percentage <=75 ){
            usability.text = 'Dni: '+Math.round(dL);
            usability.class = 'primary';
          }else{
            usability.text = 'Dni: '+Math.round(dL);
            usability.class = 'success'
          }
        }
        return usability;
      }
    };
  });
