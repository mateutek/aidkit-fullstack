'use strict';

describe('Service: aidkitMoment', function () {

  // load the service's module
  beforeEach(module('aidkitApp'));

  // instantiate service
  var aidkitMoment;
  beforeEach(inject(function (_aidkitMoment_) {
    aidkitMoment = _aidkitMoment_;
  }));

  it('should do something', function () {
    expect(!!aidkitMoment).toBe(true);
  });

});
