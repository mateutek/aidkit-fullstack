'use strict';

angular.module('aidkitApp')
  .directive('usability',['aidkitMoment', function (aidkitMoment) {
    return {
      templateUrl: 'components/usability/usability.html',
      restrict: 'EA',
      link: function (scope, element, attrs) {
        var getDiff = function(){
          scope.usability= aidkitMoment.getDiff(attrs.endDate,attrs.startDate);
        };
        //TODO:Swtich to only 1 attr
        attrs.$observe("startDate", getDiff);
        //attrs.$observe("endDate", getDiff);
      }
    };
  }]);
