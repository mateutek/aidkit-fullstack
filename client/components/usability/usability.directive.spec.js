'use strict';

describe('Directive: usability', function () {

  // load the directive's module and view
  beforeEach(module('aidkitApp'));
  beforeEach(module('components/usability/usability.html'));

  var element, scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<usability></usability>');
    element = $compile(element)(scope);
    scope.$apply();
    expect(element.text()).toBe('this is the usability directive');
  }));
});