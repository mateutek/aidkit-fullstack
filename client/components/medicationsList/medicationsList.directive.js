'use strict';

angular.module('aidkitApp')
  .directive('medicationsList', function () {
    return {
      templateUrl: 'components/medicationsList/medicationsList.html',
      restrict: 'EA',
      link: function (scope, element, attrs) {
          scope.advanced = attrs.advanced == 'true';
      }
    };
  });
