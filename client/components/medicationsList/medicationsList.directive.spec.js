'use strict';

describe('Directive: medicationsList', function () {

  // load the directive's module and view
  beforeEach(module('aidkitApp'));
  beforeEach(module('components/medicationsList/medicationsList.html'));

  var element, scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<medications-list></medications-list>');
    element = $compile(element)(scope);
    scope.$apply();
    expect(element.text()).toBe('this is the medicationsList directive');
  }));
});