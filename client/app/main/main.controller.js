'use strict';

angular.module('aidkitApp')
  .controller('MainCtrl', function ($scope, $http, $state, socket) {

    $http.get('/api/medications').success(function(medications) {
      $scope.medications = medications;
      socket.syncUpdates('medication', $scope.medications);
    });

    $scope.searchMedication = function(){
      if($scope.query && $scope.query.length >=3){
        $http.post('/api/medications/find/'+$scope.query).success(function(medication) {
          $scope.medications = medication;
        });
      }
      else if(!$scope.query){
        $http.get('/api/medications').success(function(medication) {
          $scope.medications = medication;
        });
      }
    };

    $scope.advancedSearchMedication = function() {
        $state.go('.search',{'query':$scope.query});
    }

  });
