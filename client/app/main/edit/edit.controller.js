'use strict';

angular.module('aidkitApp')
  .controller('EditCtrl', function ($scope, $http, $stateParams, $state, moment, socket) {
    $scope.dateOptions = {
      startingDay: 1,
      format:'dd.MM.yyyy',
      'closeText':'Zamknij',
      'clearText':'Wyczyść',
      'currentText':'Dzisiaj',
      'showWeeks':false
    };

    $scope.userTypes=[
      {
        value:1,
        name:'Mężczyzna/Dorosły'
      },
      {
        value: 2,
        name: 'Kobieta'
      },
      {
        value: 3,
        name: 'Dziecko'
      }
    ];

    if($stateParams.medicationId){
      $http.get('/api/medications/'+$stateParams.medicationId).success(function(medication) {
        $scope.medication = medication;
        if($scope.medication) {
          $scope.model = angular.copy($scope.medication);
          $scope.buyMaxDate = new moment();
          $scope.expirationMinDate = angular.copy($scope.buyMaxDate);
        }
      });
    }

    $scope.save = function() {
      //if($scope.model.roomId) {
      //  angular.extend($scope.room, $scope.model);
      //} else {
        $http.put('/api/medications/'+$scope.model._id,$scope.model).success(function(medication) {
          $state.go('^');
        });
      //}
    };

    $scope.open = function($event,target) {
      $event.preventDefault();
      $event.stopPropagation();
      switch (target) {
        case 'buy':
          $scope.buyOpened = true;
          break;
        case 'expiration':
          $scope.expirationOpened = true;
      }
    };

  });
