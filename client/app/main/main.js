'use strict';

angular.module('aidkitApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('main', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'MainCtrl',
        ncyBreadcrumb: {
          label: 'Spis Leków'
        }
      })
      .state('main.detail',{
        url: '{medicationId}',
        views: {
          "@" : {
            templateUrl: 'app/main/details.html',
            controller: 'MainDetailsCtrl'
          }
        },
        ncyBreadcrumb: {
          label: '{{medication.name}}'
        }
      }).state('main.detail.edit', {
        url: '/edit',
        views: {
          "@" : {
            templateUrl: 'app/main/edit/medication_form.html',
            controller: 'EditCtrl'
          }
        },
        ncyBreadcrumb: {
          label: 'Edycja'
        }
      });
  });
