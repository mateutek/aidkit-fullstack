'use strict';

angular.module('aidkitApp')
  .controller('MainDetailsCtrl',function ($scope, $http, $stateParams, socket, $state) {

    $http.get('/api/medications/'+$stateParams.medicationId).success(function(medication) {
      $scope.medication = medication;
    });

    $http.get('/api/medications').success(function(similar){
      $scope.similar = similar;
      socket.syncUpdates('medication',$scope.similar, function(event, item, object) {
        $scope.medication = item;
      });
    });

    $scope.sendToArchive = function(){
      var archiveModel = {archive:true};
      $http.put('/api/medications/'+$scope.medication._id,archiveModel).success(function(medication) {
        $state.go($state.current, {}, {reload: true});
      });
    };

    $scope.restoreFromArchive = function(){
      var archiveModel = {archive:false};
      $http.put('/api/medications/'+$scope.medication._id,archiveModel).success(function(medication) {
        $state.go($state.current, {}, {reload: true});
      });
    }

  });
