'use strict';

angular.module('aidkitApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('login', {
                url: '/login',
                templateUrl: 'app/account/login/login.html',
                controller: 'LoginCtrl',
                ncyBreadcrumb: {
                  label: 'Logowanie'
                }
            })
            .state('signup', {
                url: '/signup',
                templateUrl: 'app/account/signup/signup.html',
                controller: 'SignupCtrl',
                ncyBreadcrumb: {
                  label: 'Rejestracja'
                }
            })
            .state('settings', {
                url: '/settings',
                templateUrl: 'app/account/settings/settings.html',
                controller: 'SettingsCtrl',
                authenticate: true,
                ncyBreadcrumb: {
                  label: 'Ustawienia'
                }
            });
    });
