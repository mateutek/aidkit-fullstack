'use strict';

angular.module('aidkitApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('lists', {
        url: '/lists/all',
        views: {
          "@" : {
            templateUrl: 'app/list/list.html',
            controller: 'ListCtrl'
          }
        },
        ncyBreadcrumb: {
          label: 'Listy'
        }
      })
  });
