'use strict';

angular.module('aidkitApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.search', {
        url: 'search/{query:.*}',
        views: {
            "@" : {
                templateUrl: 'app/search/search.html',
                controller: 'SearchCtrl'
            }
        },
        params:{query:null},
        ncyBreadcrumb: {
            label: 'Wyszukiwanie zaawansowane'
        }
      });
  });
