'use strict';

angular.module('aidkitApp')
  .controller('SearchCtrl', function ($scope, $http, $stateParams) {
    $scope.advancedSearch = {};
    $scope.advancedSearch.query= $stateParams.query ? $stateParams.query : "";
    $scope.advancedSearch.searchFields = {
        antibiotic:false,
        catarrh:false,
        cough:false,
        fever:false,
        kids:false,
        adults:false,
        archive:false
    };
    $scope.searchMedication = function(){
        var searchFields = checkJson($scope.advancedSearch.searchFields);
        if(($scope.advancedSearch.query && $scope.advancedSearch.query.length>=3) || searchFields){
            $http.post('/api/medications/find/',$scope.advancedSearch).success(function(medication) {
                $scope.medications = medication;
            });
        }
        else if(!$scope.advancedSearch.query || !searchFields){
            $scope.medications = [];
        }
    };

    if($stateParams.query){
        $scope.searchMedication();
    }
  });

function checkJson(obj){
  if(typeof obj !== 'undefined'){
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) {
        if(obj[key]) {
          return true;
        }
      }
    }
    return false;
  }else{
    return false;
  }

}
