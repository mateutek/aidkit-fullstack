'use strict';

angular.module('aidkitApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('schedules', {
        url: '/schedules/all',
        views: {
          "@" : {
            templateUrl: 'app/schedule/schedule.html',
            controller: 'ScheduleCtrl'
          }
        },
        ncyBreadcrumb: {
          label: 'Rozpiski'
        }
      })
  });
