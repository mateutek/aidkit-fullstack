'use strict';

angular.module('aidkitApp')
  .controller('ScheduleCtrl', function ($scope, $http, socket) {
    $http.get('/api/schedules').success(function(schedules) {
      $scope.schedules = schedules;
      socket.syncUpdates('schedule', $scope.schedules);
    });
  });
